from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "display_page.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list": list,
    }
    return render(request, "details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            newform = form.save()
            return redirect("todo_list_detail", id=newform.id)
    else:
        form = ListForm()
    context = {"form": form}
    return render(request, "create-list.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = ListForm(instance=list)
    context = {"form": form, "list": list}
    return render(request, "edit-list.html", context)


def todo_list_delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")
    context = {"list": list}
    return render(request, "delete-list.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            newitem = form.save()
            return redirect("todo_list_detail", id=newitem.list.id)
    else:
        form = ItemForm()
    context = {"form": form}
    return render(request, "create-task.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm(instance=item)
    context = {
        "item": item,
        "form": form,
    }
    return render(request, "edit-task.html", context)
