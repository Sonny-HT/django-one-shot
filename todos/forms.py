from django.forms import ModelForm
from .models import TodoList, TodoItem


class ListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class ItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
