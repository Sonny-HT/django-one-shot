# Generated by Django 4.1.7 on 2023-03-01 20:14

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="due_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
